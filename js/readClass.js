//This is an open source script, please use at your own will.
//Also, if you dont mind, visit my site (tylerjefford.com)
//Enjoy

$(document).ready(function(){

	var words = $('.readClass').text();
	var wordcount = $.trim(words).split(" ").length;
	
	var m = Math.floor(wordcount / 200);
	var s = Math.floor(wordcount % 200 / (200 / 60));
	var est = m  + ' minute' + (m == 1 ? '' : 's') + ', ' +s + ' second' +(s == 1 ? '' : 's');
	
	$('#estimated_time').html(est);
	
});