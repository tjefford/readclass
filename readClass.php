<?php

//This is an open source script, please use at your own will.
//Also, if you dont mind, visit my site (tylerjefford.com)
//Enjoy

	class read {
		
		function estimateTime($word){
		
			$word = str_word_count(strip_tags($word));
			$m = floor($word / 200);
			$s = floor($word % 200 / (200 / 60));
			$est = $m . ' minute' . ($m == 1 ? '' : 's') . ', ' . $s . ' second' . ($s == 1 ? '' : 's');
			
			return $est;
		}
			
	}
		
?>